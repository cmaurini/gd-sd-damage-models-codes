# Strain gradient damages models

### Description

* This repository contains the codes for solving strain-gradient damage models, and is intended as supplementary material of the paper
  - Strain-gradient vs damage-gradient regularizations of softening damage models by Duc Trung Le, Jean-Jacques Marigo, Corrado Maurini, Stefano Vidoli
  which is currently submitted for publication.
* Version 0.1

### Basic informations

* The codes depend on FEniCS version 2017.1.0 and h5py, we reccomand to run the code in a docker
  virtual machine. To enter the appropiate virtual machine run at the bash command line

    `source launch-container.sh`

    You can execute then the code below within the virtual machine, sharing the repository directory with your own OS, where you can visualize the results. See also https://fenicsproject.org for instructions on fenics on docker.

* To test the code on strain gradient model

	```
	python scr/sgdam.py
    ```

* You can reproduce the figures of the paper on strain gradient models by running the following scripts:

    - You can find here the result files used to generate the figures for the paper:

        https://bitbucket.org/cmaurini/gd-sd-damage-models-codes/downloads/results4paper.zip

        To generate the figures:

        1. Download and unzip the zip archive in the main directory of the repository. This should create the `results4paper` directory.

        2. Run the scripts below.

    - For generating the figures:

	     `python plot-energies.py && python plot-energy-mesh.py && python plot-fields.py`

    - For re-running the simulations and parametric analyses (it takes some time):

         `python launcher-G1.py && python launcher-G0.py`



### Content

Descriptions of the file in `src`:

- `src/sgdam.py` contains the implementation of strain gradient damage models in 1d used in the paper and is the core of this repository.

- `src/sgdam.py` contains an implementation of damage-gradient damage models in 1d. It has been used for comparisons and provided here as basic reference, but better and more complete implementations can be found here: `https://bitbucket.org/cmaurini/gradient-damage`

- `src/postptocessing_local.py` contains utility functions

- `src/2D` contains experimental implementation of strain gradient damage models in 2D antiplane elasticity. The codes in this directory have not be verified and will not be maintaned.


### Contact

For any question or for contribution, contact:

Corrado Maurini: corrado.maurini@upmcfr



### License

This code is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with fenics-shells.  If not, see http://www.gnu.org/licenses/.
