from fenics import *
parameters.plotting_backend = "matplotlib"
#parameters.form_compiler.log_level = 0
import matplotlib.pyplot as plt
import h5py
import os, sys
import numpy as np
sys.path.append("src/")
from postprocessing_local import *
import matplotlib
font = {'size'   : 14}
matplotlib.rc('font', **font)

for name in [   "SG-NS-G1-0100-eta2.00e-01",
                "SG-LS-G1-0100-eta2.00e-01"]:
    plt.figure()
    filename='results4paper/%s/output.h5'%name
    fileh5py = h5py.File(filename,'r')
    loads = fileh5py.get('loads').value
    dissipated_energy = fileh5py.get('dissipated_energy').value
    local_elastic_energy = fileh5py.get('local_elastic_energy').value
    nonlocal_elastic_energy = fileh5py.get('nonlocal_elastic_energy').value
    plt.plot(loads,local_elastic_energy, label="local_elastic_energy", ls="dotted", marker = "", color = "gray",linewidth=3.0)
    plt.plot(loads,nonlocal_elastic_energy, label="local_elastic_energy",ls="dashed", marker = "", color = "gray",linewidth=3.0)
    plt.plot(loads,dissipated_energy, label="dissipated_energy", marker = "", color = "black",linewidth=3.0)
    plt.xlabel("load (t)")
    plt.ylabel("Energies")
    plt.legend(["Local elastic energy", "nonlocal elastic energy", "Dissipated energy"], loc="upper right")
    plt.xlim(0,5)
    plt.ylim(0,1.5)
    plt.savefig("figures/energies-%s.pdf"%name)

