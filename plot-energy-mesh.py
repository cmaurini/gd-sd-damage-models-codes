import matplotlib.pyplot as plt
import h5py
import os, sys
import numpy as np
#plt.style.use('ggplot')
#sys.path.append("src/")
nel_values = np.array([30,40,50,60,70,80,90,100,125,150,175,200,250,300])
nval = ["%.4d"%n for n in  nel_values]
meshsizes=[1./int(n) for n in nval]
energyval = [h5py.File('results4paper/SG-NS-GE-%s-eta2.00e-01/output.h5'%n,'r').get('dissipated_energy').value[-1] for n in nval ]
plt.loglog(meshsizes,energyval,'d',label="NS-G0")
energyval = [h5py.File('results4paper/SG-LS-GE-%s-eta2.00e-01/output.h5'%n,'r').get('dissipated_energy').value[-1] for n in nval ]
plt.loglog(meshsizes,energyval,'o',label="LS-G0",mfc='none')
energyval = [h5py.File('results4paper/SG-NS-G1-%s-eta2.00e-01/output.h5'%n,'r').get('dissipated_energy').value[-1] for n in nval ]
plt.loglog(meshsizes,energyval,'^',label="NS-G1")
energyval = [h5py.File('results4paper/SG-LS-G1-%s-eta2.00e-01/output.h5'%n,'r').get('dissipated_energy').value[-1] for n in nval ]
plt.loglog(meshsizes,energyval,'.',label="LS-G1")
for nloc in [4,5]:
    val = nloc*np.array(meshsizes,dtype="float")
    plt.loglog(meshsizes,val,color='gray',ls="--",label="%s elements"%nloc)
plt.loglog(meshsizes,np.ones(len(meshsizes)),ls="-",color='gray',label="Entire bar",linewidth="2")
plt.xlabel("Mesh size")
plt.ylabel("Dissipated energy")
#plt.legend(frameon=False,loc="center", bbox_to_anchor=(1.2, 0.5))
plt.legend(frameon=True,loc="lower right")
plt.xlim([1./335.,1./30])
plt.ylim([.005,2])
plt.tight_layout()
plt.savefig("figures/energies_vs_mesh_sg.pdf")
