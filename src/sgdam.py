# Copyright (C) 2017 Corrado Maurini
#
# This file is part of sgdg-gradientdamage.
#
# This code is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This code is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this repository. If not, see <http://www.gnu.org/licenses/>.

# Created  2/7/2015 by Corrado Maurini

from dolfin import *
import numpy as np
import math
import sympy
import matplotlib.pyplot as plt
import pylab
import os
import shutil
set_log_level(ERROR)
import matplotlib
matplotlib.interactive(True)
#----------------------------------------------------------------------------
# Parameters
#----------------------------------------------------------------------------

def sg_solver(user_par):
    #parameters["plotting_backend"] = "matplotlib"
    #user_par = parameters.user
    parameters.parse()
    default_tao_solver_parameters = {"maximum_iterations": 200,
                             "report": False,
                             "line_search": "more-thuente", # [armijo, default, gpcg, more-thuente, owarmijo, unit]
                             "linear_solver": "mumps",
                             "method":"tron",
                             "gradient_absolute_tol": 1.e-8,
                             "gradient_relative_tol": 1.e-8,
                             "error_on_nonconvergence": True}
    # Loading
    u0 = Constant(0.0)
    uL = Expression("t", t = 0., degree=0)
    b = Constant(0.0) # bulk load
    # Parameters of the damage model
    w_1, E_0, = 1., 1.
    eta = user_par.eta # internal length
    k = user_par.k
    kresE = user_par.kresE
    kresG = user_par.kresG
    model = user_par.model
    # Constitutive functions of the damage model
    if model == "LS":
        w = lambda z: z #1- (1-z)**2
        E = lambda z: (1-w(z))/(1+(k-1)*w(z))
    elif model == "NS":
        w = lambda z: z
        E = lambda z: (1-z)**2
    if user_par.G == 1:
        G = Constant(1.)
        prefix = "SG-%s-G1"%model
    else:
        G = lambda z: E(z)
        prefix =  "SG-%s-GE"%model
    # File to store data
    savedir = "%s/%s-%s"%(user_par.root_dir,prefix,user_par.suffix)
    if os.path.isdir(savedir):
        shutil.rmtree(savedir)
    File(savedir+'/parameters.xml') << parameters
    output_file_h5  = HDF5File(mpi_comm_world(), savedir+"/output.h5", "w")
    output_file_pvd = File(savedir+"/output.pvd")
    time_data_file = open(savedir+"/data.txt", "w+")
    time_data_file.write("%s %s %s %s %s %s %s \n" % ("t", "stress", "Length Damaged Zone", "Elastic Energy Local", "Elastic Energy Nonlocal", "Dissipated Energy", "Total Energy"))
    #----------------------------------------------------------------------------
    # Create mesh and function space
    #----------------------------------------------------------------------------
    n_el = user_par.n_element
    mesh = UnitIntervalMesh(n_el)
    mesh_disp = Function(VectorFunctionSpace(mesh,"CG",1))
    rand = np.random.rand(mesh_disp.vector().size())
    rand[0] = 0.; rand[-1]=0.;
    mesh_disp.vector()[:] = 3.e-3*mesh.hmin()*rand
    ALE.move(mesh, mesh_disp)
    output_file_h5.write(mesh, "mesh")
    CG2 = FiniteElement("CG", mesh.ufl_cell(), 2)
    CG1 = FiniteElement("CG", mesh.ufl_cell(), 1)
    DG0 = FiniteElement("DG", mesh.ufl_cell(), 0)
    V_elast = FunctionSpace(mesh, MixedElement((CG2, CG1, DG0)))
    V_alpha = FunctionSpace(mesh, DG0) # damage
    #----------------------------------------------------------------------------
    # Define boundary sets for boundary conditions
    #----------------------------------------------------------------------------
    class Left(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[0], 0.)

    class Right(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[0], 1.0)
    # Define boundary condition
    bcu_0 = DirichletBC(V_elast.sub(0), u0, Left()) # boundary condition for u at x=0
    bcu_1 = DirichletBC(V_elast.sub(0), uL, Right()) # imposed displacement at x=1
    bceps_0 = DirichletBC(V_elast.sub(1), 0, Left()) # boundary condition for u at x=0
    bceps_1 = DirichletBC(V_elast.sub(1), 0, Right()) # boundary condition for u at x=0
    bc_elasticity = [bcu_0, bcu_1, bceps_0, bceps_1]  #Dirichlet boundary condition for a traction test boundary
    # Boundary for alpha
    bc_alpha0 = DirichletBC(V_alpha, 0., Left())
    bc_alpha1 = DirichletBC(V_alpha, 0., Right())
    bc_damage  = []#bc_alpha0, bc_alpha1]
    alpha_0 = interpolate(Expression("0.0", degree=0), V_alpha) # initial (known) alpha
    lb = alpha_0.copy(deepcopy=True) # lower bound of alpha, initialized at alpha_0
    ub = interpolate(Constant(1.), V_alpha) # upper bound of alpha, set to 1
    # Define the function, test and trial fields
    # Elasticity
    u_eps_tau = Function(V_elast)
    (u, eps, tau) = split(u_eps_tau)
    du_eps_tau = TrialFunction(V_elast)
    u_eps_tau_test = TestFunction(V_elast)
    (du, dep, dtau) = TrialFunctions(V_elast)
    (u_test, eps_test, tau_test) = TestFunctions(V_elast)
    # Damage
    alpha = Function(V_alpha)
    dalpha = TrialFunction(V_alpha)
    alpha_test = TestFunction(V_alpha)
    # Energy density
    imperfection_E = Expression(
                    """(x[0] > x0 - d/2. && x[0] < x0) ? 1- ampl*pow((x[0]-(x0-d/2.))/(d/2.),2): (x[0] >= x0 && x[0] <= x0 + d/2) ? 1- ampl*pow((x[0]-(x0+d/2.))/(d/2.),2):1. """
                              , ampl=user_par.imp_ampl, x0=user_par.imp_pos, d=user_par.imp_d, degree=4)
    #imperfection_E = Expression('x[0] > x0 + d/2 || x[0] < x0 - d/2 ? 1. : 1-ampl', d = .01, x0=.2, ampl=user_par.imp_ampl, degree=0)
    output_file_h5.write(interpolate(imperfection_E,V_alpha), "E")
    plot(interpolate(imperfection_E,V_alpha),backend='matplotlib')
    plt.show()
    #E0 = imperfection_E*E0
    elastic_energy_local = E_0*0.5*(E(alpha)+ Constant(kresE))*u.dx(0)**2
    elastic_energy_nonlocal = 0.5*E_0*Constant(eta)**2*(G(alpha)+Constant(kresG))*eps.dx(0)**2
    elastic_energy = elastic_energy_local + elastic_energy_nonlocal
    dissipated_energy = imperfection_E*(Constant(w_1))*w(alpha)
    external_work = b * u
    strain_energy = elastic_energy + dissipated_energy

    # Functional for the variational formulation
    # Enforce the constraint u.dx = eps with the lagrange multiplier tau (which is a double stress)
    constraint = (eps - u.dx(0)) * tau
    En = strain_energy*dx - external_work*dx + constraint*dx

    # The stress
    sigma = Constant(E_0)*imperfection_E*(E(alpha)*u.dx(0) - tau)

    #  Weak form of elasticity problem
    En_u = derivative(En, u_eps_tau, u_eps_tau_test)
    J_u = derivative(En_u, u_eps_tau, du_eps_tau)
    weak_form_elasticity = replace(En_u, {u_eps_tau: du_eps_tau})

    # Weak form of damage problem
    En_alpha = derivative(En, alpha, alpha_test)
    J_alpha = derivative(En_alpha, alpha, dalpha)

    #----------------------------------------------------------------------------
    # Set up the variational problems and solvers
    #----------------------------------------------------------------------------
    # Elasticity
    problem_elasticity = LinearVariationalProblem(lhs(weak_form_elasticity), rhs(weak_form_elasticity), u_eps_tau, bc_elasticity)
    solver_elasticity = LinearVariationalSolver(problem_elasticity)
    solver_elasticity.parameters.linear_solver = "mumps"
    # Damage
    class DamageProblem(OptimisationProblem):

        def __init__(self):
            OptimisationProblem.__init__(self)
            self.total_energy = En
            self.Dalpha_total_energy = En_alpha
            self.J_alpha = J_alpha
            self.alpha = alpha
            self.bc_alpha = bc_damage

        def f(self, x):
            self.alpha.vector()[:] = x
            return assemble(self.total_energy)

        def F(self, b, x):
            self.alpha.vector()[:] = x
            assemble(self.Dalpha_total_energy, b)
            for bc in self.bc_alpha:
                bc.apply(b)

        def J(self, A, x):
            self.alpha.vector()[:] = x
            assemble(self.J_alpha, A)
            for bc in self.bc_alpha:
                bc.apply(A)

    solver_damage = PETScTAOSolver()
    solver_damage.parameters.update(default_tao_solver_parameters)
    alpha_error = Function(V_alpha)
    info(solver_damage.parameters,True)
    maxiter = 100
    toll = 1.e-5
    t = 0.0
    loads = np.linspace(user_par.tmin, user_par.tmax, user_par.nsteps)
    elastic_energy_local_out = 0*loads
    elastic_energy_nonlocal_out = 0*loads
    dissipated_energy_out = 0*loads
    stress_out = 0*loads
    damaged_zone_length_out = 0*loads
    errors = np.zeros((len(loads),maxiter))
    output_file_h5.write(loads, "loads")
    # Time stepping
    for (i, t) in enumerate(loads):
        # parameters of the alternate minimization
        iter = 1; err_alpha = 1
        lap = 1; L_total = 0.0
        uL.t = t

        # Solving at each timestep with alternate minimization
        while err_alpha > toll and iter < maxiter:
            # solve elastic problem
            solver_elasticity.solve()
            solver_damage.solve(DamageProblem(),alpha.vector(),lb.vector(), ub.vector())
            alpha_error.vector()[:] = alpha.vector() - alpha_0.vector()
            err_alpha = np.linalg.norm(alpha_error.vector().array(), ord = np.Inf) # test error
            alpha_0.assign(alpha) # update
            print("Step t = %2.4e, iter: %s, error:%2.4e"%(t,iter,err_alpha))
            iter=iter+1
        # plot damaged zone
        np.savetxt(savedir+"/errors.txt",errors)
        #updating the lower bound to account for the irreversibility
        lb.vector()[:] = alpha.vector()
        (us, epss, taus) = u_eps_tau.split(deepcopy= True)
        plt.figure(2)
        plot(alpha, backend="matplotlib")
        plt.ylim([0.,1.1])
        plt.draw()
        plt.pause(0.0001)
        plt.figure(1)
        plot(us, backend="matplotlib")
        plt.ylim([0.,5.])
        plt.draw()
        plt.pause(0.0001)
        alpha.rename('alpha','damage')
        us.rename('u','displacement')
        epss.rename('eps','strain')
        taus.rename('tau','double stress')
        sigmas = project(sigma,FunctionSpace(mesh, DG0),solver_type="umfpack")
        sigmas.rename('sigma','stress')
        damaged_zone_length = assemble(conditional(alpha > 1e-8, 1., 0.)*dx)
        # Save
        for field in [alpha,us,epss,taus,sigmas]:
            output_file_pvd << (alpha, t)
        output_file_h5.write(alpha,"alpha",t)
        output_file_h5.write(us,"u",t)
        output_file_h5.write(epss,"eps",t)
        output_file_h5.write(taus,"tau",t)
        output_file_h5.write(sigmas,"sigma",t)

        #----------------------------------------------------------------------------
        # Some post-processing
        #----------------------------------------------------------------------------
        dissipated_energy_out[i] = assemble(dissipated_energy*dx) # dissipated energy
        elastic_energy_local_out[i] = assemble(elastic_energy_local*dx) #elastic energy
        elastic_energy_nonlocal_out[i] = assemble(elastic_energy_nonlocal*dx) #elastic energy
        ener_total = assemble(En) # total energy
        stress_out[i] = sigmas(1.)
        damaged_zone_length_out[i] = damaged_zone_length
        ener_total = assemble(En) # total energy
        #Write to files
        time_data_file.write("%s %s %s %s %s %s %s \n" %(t,
                                                         sigmas(1.),
                                                         damaged_zone_length, elastic_energy_local_out[i],
                                                         elastic_energy_nonlocal_out[i],
                                                         dissipated_energy_out[i],
                                                         ener_total))

    output_file_h5.write(elastic_energy_local_out, "local_elastic_energy")
    output_file_h5.write(elastic_energy_nonlocal_out, "nonlocal_elastic_energy")
    output_file_h5.write(dissipated_energy_out, "dissipated_energy")
    output_file_h5.write(stress_out, "stress")
    output_file_h5.write(damaged_zone_length_out, "damaged_zone")
    time_data_file.close()
    output_file_h5.close()
    del output_file_h5, output_file_pvd
    plt.close("all")

if __name__ == '__main__':
    user_par = Parameters("user", model=("NS",["LS","NS"]),
                          G=1, n_element=100, eta=0.2, k=2., #material data and number of element
                          kresE=1.e-5, kresG=1.e-8,  # residual stiffnesses
                          imp_d=0.0, imp_ampl=0.0, imp_pos=0.4, # data on imperfections
                          suffix="test", root_dir="results",  # output dir#
                          tmin = 0., tmax = 8., nsteps = 81  # time-stepping
                          )
    sg_solver(user_par)

