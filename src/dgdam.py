# Copyright (C) 2017 Corrado Maurini
#
# This file is part of sgdg-gradientdamage.
#
# This code is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This code is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this repository. If not, see <http://www.gnu.org/licenses/>.

# Strain gradient damage model for constant strain-gradient stiffness
# Created 2/7/2015 by C.Maurini


from dolfin import *
import numpy as np
import math, sympy
import matplotlib.pyplot as plt
import os, shutil, uuid
parameters.plotting_backend = 'matplotlib'

#----------------------------------------------------------------------------
# Parameters
#----------------------------------------------------------------------------
user_par = Parameters("user", model = ("LS", ["LS", "NS"]), k = 2., eta=0.05, kres = 1.e-4, n_element = 100, imp_d=0.01, imp_ampl = 0.1, imp_pos = 0.6, suffix= "", root_dir="results")
user_par.add("tmin", 0.)
user_par.add("tmax", 4.)
user_par.add("nsteps", 100.)
try:
    parameters.add(user_par)
except:
    print("user parameters already defined")
parameters.parse()
user_par = parameters.user
# Parameters of the nonlinear solver used for the alpha-problem
snes_solver_parameters_bounds = {"nonlinear_solver": "snes",
                          "symmetric": True,
                          "snes_solver": {"maximum_iterations": 100,
                                          "report": True,
                                          "line_search": "basic",
                                          "linear_solver": "umfpack",
                                          "method":"vinewtonrsls",
                                          "absolute_tolerance": 1e-9,
                                          "relative_tolerance": 1e-9,
                                          "error_on_nonconvergence": True}}

tao_solver_parameters = {"maximum_iterations": 100,
                         "report": True,
#                         "line_search": "armijo",
                         "linear_solver": "mumps",
                         "method":"tron",
                         "gradient_absolute_tol": 1e-7,
                         "gradient_relative_tol": 1e-7,
                         "error_on_nonconvergence": True}
# Loading
u0 = Constant(0.0)
uL = Expression("t", t = 0., degree = 0)
u_step = 0.05 # load step
u_max = 15. # maximum imposed displacement
b = Constant(0.) # bulk load
# Parameters of the damage model
w_1, E_0, = 1., 1.
eta = user_par.eta # internal length
k = user_par.k
model = user_par.model
kres  = user_par.kres
# Constitutive functions of the damage model
if model == "LS":
    w = lambda z: z #1- (1-z)**2
    E = lambda z: (1-w(z))/(1+(k-1)*w(z)) + Constant(kres)
elif model == "NS":
    w = lambda z: z
    E = lambda z: (1-z)**2 + Constant(kres)
# File to store data
n_el = user_par.n_element
if eta == 0.:
    prefix = "Local-%s"%model
else:
    prefix = "DG-%s"%model
savedir = "%s/%s-%s"%(user_par.root_dir,prefix,user_par.suffix)
if os.path.isdir(savedir):
    shutil.rmtree(savedir)
File(savedir+'/parameters.xml') << parameters
output_file_h5  = HDF5File(mpi_comm_world(), savedir+"/output.h5", "w")
output_file_xmdf = File(savedir+"/output.pvd")
time_data_file = open(savedir+"/data.txt", "w+")
time_data_file.write("%s %s %s %s %s %s %s \n" % ("t", "stress", "Length Damaged Zone", "Elastic Energy Local", "Elastic Energy Nonlocal", "Dissipated Energy", "Total Energy"))
damage_txt = open(savedir+"/data.txt", "w+")
strain_txt = open(savedir+"/data.txt", "w+")
#file_alpha = File(savedir+"/damage/alpha.pvd")
#----------------------------------------------------------------------------
# Create mesh and function space
#----------------------------------------------------------------------------
mesh = UnitIntervalMesh(n_el)
mesh_disp = Function(FunctionSpace(mesh,"CG",1))
#rand = np.random.rand(mesh_disp.vector().size())
#rand[0] = 0.; rand[-1]=0.;
#mesh_disp.vector()[:] = 1.e-1*mesh.hmin()*rand
#ALE.move(mesh, mesh_disp)
output_file_h5.write(mesh, "mesh")
V_u = FunctionSpace(mesh, "CG", 1) # displacement (u)
# V_eta = FunctionSpace(mesh, "CG",1) # strain (epsilon)
if eta == 0.:
    V_alpha = FunctionSpace(mesh, "DG", 0)
else:
    V_alpha = FunctionSpace(mesh, "CG", 1)
#V_tau = FunctionSpace(mesh, "DG", 0) # lagrange multiplier (double stress)
V_elast = V_u # Mixed funtion space for gradient elasticity
#----------------------------------------------------------------------------
# Define boundary sets for boundary conditions
#----------------------------------------------------------------------------
# Sub domain for Dirichlet boundary condition
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.)
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0)
# Define boundary condition
bcu_0 = DirichletBC(V_u, u0, Left()) # boundary condition for u at x=0
bcu_1 = DirichletBC(V_u, uL, Right()) # imposed displacement at x=1
bc_elasticity = [bcu_0, bcu_1]  #Dirichlet boundary condition for a traction test boundary
# Boundary for alpha
bc_alpha0 = DirichletBC(V_alpha, 0., Left())
bc_alpha1 = DirichletBC(V_alpha, 0., Right())
bc_damage  = []#bc_alpha0, bc_alpha1]
lb = interpolate(Expression("0.0", degree=0), V_alpha) # lower bound of alpha, initialized at alpha_0
ub = interpolate(Constant(1.), V_alpha) # upper bound of alpha, set to 1
# Define the function, test and trial fields
# Elasticity
u = Function(V_u)
u_test = TestFunction(V_elast)
du = TrialFunction(V_elast)
# Damage
alpha =  Function(V_alpha) #interpolate(imperfection, FunctionSpace(mesh,'DG',0)) #
dalpha = TrialFunction(V_alpha)
alpha_test = TestFunction(V_alpha)
# Energy density
imperfection_E = Expression("""(x[0] > x0 - d/2. && x[0] < x0)
                          ?
                          1- ampl*pow((x[0]-(x0-d/2.))/(d/2.),2)
                          :
                          (x[0] >= x0 && x[0] <= x0 + d/2)
                              ?
                              1- ampl*pow((x[0]-(x0+d/2.))/(d/2.),2)
                              :
                              1.
                          """, ampl=user_par.imp_ampl, x0=user_par.imp_pos, d=user_par.imp_d, degree=4)
#imperfection_E = Expression("1-ampl*exp(-pow((x[0]-x0)/d,2))", ampl=user_par.imp_ampl, x0=user_par.imp_pos, d=user_par.imp_d, degree=4)  #Expression('x[0] > x0 + d/2 || x[0] < x0 - d/2 ? 1. : E_min', d = .01, x0=.2, E_min=1-1.e-5, degree=0)
output_file_h5.write(interpolate(imperfection_E,V_alpha), "E")
elastic_energy_local = 0.5*imperfection_E*E(alpha)*u.dx(0)**2
elastic_energy = elastic_energy_local
dissipated_energy = Constant(w_1) * (w(alpha) + (Constant(eta)**2)/2.*alpha.dx(0)**2)
external_work = b * u
strain_energy = elastic_energy + dissipated_energy
# Functional for the variational formulation
En = strain_energy*dx - external_work*dx
# The stress
sigma = Constant(E_0)*(E(alpha)*u.dx(0))
#  Weak form of elasticity problem
En_u = derivative(En, u, u_test)
J_u = derivative(En_u, u, du)
weak_form_elasticity = replace(En_u, {u: du})
# Weak form of damage problem
En_alpha = derivative(En, alpha, alpha_test)
J_alpha = derivative(En_alpha, alpha, dalpha)
#----------------------------------------------------------------------------
# Set up the variational problems and solvers
#----------------------------------------------------------------------------
# Elasticity
problem_elasticity = LinearVariationalProblem(lhs(weak_form_elasticity), rhs(weak_form_elasticity), u, bc_elasticity)
solver_elasticity = LinearVariationalSolver(problem_elasticity)
solver_elasticity.parameters.linear_solver = "umfpack"
# Damage
class DamageProblem(OptimisationProblem):

    def __init__(self):
        OptimisationProblem.__init__(self)
        self.total_energy = En
        self.Dalpha_total_energy = En_alpha
        self.J_alpha = J_alpha
        self.alpha = alpha
        self.bc_alpha = bc_damage

    def f(self, x):
        self.alpha.vector()[:] = x
        return assemble(self.total_energy)

    def F(self, b, x):
        self.alpha.vector()[:] = x
        assemble(self.Dalpha_total_energy, b)
        for bc in self.bc_alpha:
            bc.apply(b)

    def J(self, A, x):
        self.alpha.vector()[:] = x
        assemble(self.J_alpha, A)
        for bc in self.bc_alpha:
            bc.apply(A)

solver_damage = PETScTAOSolver()
solver_damage.parameters.update(tao_solver_parameters)
#problem_damage = NonlinearVariationalProblem(En_alpha, alpha, bc_damage, J_alpha)
#solver_damage = NonlinearVariationalSolver(problem_damage)
#solver_damage.parameters.update(snes_solver_parameters_bounds)
alpha_error = Function(V_alpha)
maxiter = 500
toll = 1.e-8
t = .0
# hitory data
loads = np.linspace(user_par.tmin, user_par.tmax, user_par.nsteps)
elastic_energy_out = 0*loads
dissipated_energy_out = 0*loads
stress_out = 0*loads
damaged_zone_length_out = 0*loads
output_file_h5.write(loads, "loads")
alpha_0 = Function(V_alpha)
errors = np.zeros((len(loads),maxiter))
# Time stepping
for (i, t) in enumerate(loads):
    # parameters of the alternate minimization
    iter = 1; err_alpha = 1
    lap = 1; L_total = 0.0
    uL.t = t
    # Solving at each timestep with alternate minimization
    while err_alpha > toll and iter < maxiter:
        #plot(alpha,interactive=True)
        solver_elasticity.solve() # solve elastic problem
        #problem_damage.set_bounds(lb.vector(),ub.vector())
        #solver_damage.solve() # solve damage problem
        solver_damage.solve(DamageProblem(),alpha.vector(),lb.vector(), ub.vector())
        alpha_error.vector()[:] = alpha.vector() - alpha_0.vector()
        err_alpha = np.linalg.norm(alpha_error.vector().array(), ord = np.Inf) # test error
        alpha_0 = alpha.copy(deepcopy = True) # update
        errors[i,iter-1] = err_alpha
        print("Step t = %2.4e, iter: %s, error:%2.4e"%(t,iter,err_alpha))
        iter=iter+1
    # plot damaged zone
    #plot(alpha, key = "alpha", title = "Damage at loading %.4f"%(t) ,range_min=0.,range_max=1.,interactive = False )
    #updating the lower bound to account for the irreversibility
    np.savetxt(savedir+"/errors.txt",errors)
    lb.vector()[:] = alpha.vector()
    alpha.rename('alpha','damage')
    u.rename('u','displacement')
    damaged_zone_length = assemble(conditional(alpha > 1e-8, 1., 0.)*dx)
    #plot(ep)
    #plot(sigma)
    plot(alpha)
    # Save in hdf5 format.
    output_file_h5.write(alpha, "alpha", t)
    output_file_h5.write(u, "u", t)
    output_file_xmdf << (u, t)
    output_file_xmdf << (alpha, t)
    #----------------------------------------------------------------------------
    # Some post-processing
    #----------------------------------------------------------------------------
    sigmas = project(sigma, FunctionSpace(mesh, "DG", 0),solver_type="umfpack")
    # Calculate the energies
    dissipated_energy_out[i] = assemble(dissipated_energy*dx) # dissipated energy
    elastic_energy_out[i] = assemble(elastic_energy_local*dx) #elastic energy
    ener_total = assemble(En) # total energy
    stress_out[i] = sigmas(1.)
    damaged_zone_length_out[i] = damaged_zone_length
    #field_file << ener_elas
    #Write to files
    time_data_file.write("%s %s %s %s %s %s \n" %(t, sigmas(1.), damaged_zone_length, elastic_energy_out[i], dissipated_energy_out[i], ener_total))
    #file_alpha << (alpha,u_t)
output_file_h5.write(elastic_energy_out, "elastic_energy")
output_file_h5.write(dissipated_energy_out, "dissipated_energy")
output_file_h5.write(stress_out, "stress")
output_file_h5.write(damaged_zone_length_out, "damaged_zone")
time_data_file.close()
del output_file_h5, output_file_xmdf


