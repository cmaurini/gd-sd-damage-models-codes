from fenics import * 
parameters.plotting_backend = "matplotlib"
import matplotlib.pyplot as plt
import h5py
import subprocess, os
import numpy as np

def initialize_fields(myHDF5File):
    # Mesh
    mesh = Mesh()
    myHDF5File.read(mesh,"mesh",True)
    u = Function(FunctionSpace(mesh, "CG", 1))
    alpha = Function(FunctionSpace(mesh, "DG", 0))
    return u, alpha

def initialize_cg1(myHDF5File):
    mesh = Mesh()
    myHDF5File.read(mesh,"mesh",True)
    cg1 = Function(FunctionSpace(mesh, "CG", 1))
    return cg1

def initialize_dg0(myHDF5File):
    mesh = Mesh()
    myHDF5File.read(mesh,"mesh",True)
    dg0 = Function(FunctionSpace(mesh, "DG", 0))
    return dg0

def update_field(field, name, myHDF5File,  step):
    myHDF5File.read(field, "%s/vector_%d"%(name,step))
    pass

def plot_imperfection(dir_name):
    file_name = dir_name + "/output.h5"
    myHDF5File = HDF5File(mpi_comm_world(), file_name, "r") 
    E = initialize_alpha(myHDF5File)
    myHDF5File.read(E, "E")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    E_plot = plot(E)
    plt.setp(E_plot, marker ="o", color='b', linewidth=3.0)
    ax.set_xlabel('x')
    ax.set_ylim(0,1.01)
    ax.set_ylabel('E')
    ax.set_aspect("auto")
    return E_plot

def plot_fields(u, alpha, loads, myHDF5File, step):
    update_field(u, "u", myHDF5File, step)
    update_field(alpha, "alpha", myHDF5File, step)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    alpha_plot = plot(alpha)
    u_plot = plot(u)
    plt.setp(alpha_plot, marker ="o", color='r', linewidth=3.0)
    plt.setp(u_plot, marker ="o", color='b', linewidth=3.0)
    ax.set_xlabel('x')
    ax.set_ylim(0,4.5)
    ax.set_ylabel('displacement and damage')
    ax.set_aspect("auto")
    ax.set_title("t = %s"%loads[step])
    return ax

def plot_energies(file_name):
    fileh5py = h5py.File(file_name,'r')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(fileh5py.get('loads').value,fileh5py.get('dissipated_energy').value, '-ro')
    plt.plot(fileh5py.get('loads').value,fileh5py.get('elastic_energy').value,'-ob')
    ax.set_xlabel('t')
    ax.set_ylabel('Enegies')
    
def show_results(dir_name, step):
    file_name = dir_name + "/output.h5"
    print(file_name)
    myHDF5File = HDF5File(mpi_comm_world(), file_name, "r") 
    fileh5py = h5py.File(file_name,'r')
    loads = fileh5py.get('loads').value
    u = initialize_u(myHDF5File)
    alpha = initialize_alpha(myHDF5File)
    ax = plot_fields(u,alpha,loads,myHDF5File,step)
    ax.set_ylim([0,2])
    plot_energies(file_name)
    plot_imperfection(dir_name)
    ax.set_ylim([0,1.2])