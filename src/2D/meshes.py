from fenics import *
from mshr import *

def CTmesh( a = .5, b = .5, c = 1., copening = 0.001, clength = .03, n_element=100):
    #----------------------------------------------------------------------------
    # Create mesh and boundary indicators
    #----------------------------------------------------------------------------
    p5 = Point(0.,-copening/2)
    p6 = Point(clength,0.)
    p7 = Point(0.,copening/2)
    p1 = Point(0,-a/2.)
    p2 = Point(0,a/2.)
    p4 = Point(b,-c/2.)
    p3 = Point(b,c/2.)
    geom = Polygon([p4,p3,p2,p7,p6,p5,p1])
    mesh = generate_mesh(geom,n_element)
    class LeftUp(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[0], 0.) and x[1] > 0.

    class LeftDown(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[0], 0.0) and x[1] < 0.

    class Right(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[0], b)
    leftup = LeftUp()
    leftdown = LeftDown()
    right = Right()
    boundary_indicator = MeshFunction('size_t', mesh, 1, 0)
    leftup.mark(boundary_indicator, 1)
    leftdown.mark(boundary_indicator, 2)
    right.mark(boundary_indicator, 3)
    return (mesh, boundary_indicator)

def Traction2dDefect(a = .5, b = 1., copening = 0.1, n_element=100):
    #----------------------------------------------------------------------------
    # Create mesh and boundary indicators
    #----------------------------------------------------------------------------
    p1 = Point(0,-a/2.)
    p2 = Point(0,a/2.)
    p4 = Point(b,-a/2.)
    p3 = Point(b,a/2.)
    geom1 = Polygon([p4,p3,p2,p1])
    geom2 = Circle(Point(0.,0.),copening)
    mesh = generate_mesh(geom1-geom2,n_element)
    class Up(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[1], a/2.)

    class Bottom(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[1], -a/2.)

    up = Up()
    bottom = Bottom()
    boundary_indicator = MeshFunction('size_t', mesh, 1, 0)
    up.mark(boundary_indicator, 1)
    bottom.mark(boundary_indicator, 2)
    return (mesh, boundary_indicator)

def Traction2dDefectSym(a = .5, b = .7, copening = 0.1, n_element=100):
    #----------------------------------------------------------------------------
    # Create mesh and boundary indicators
    #----------------------------------------------------------------------------
    p1 = Point(0,-a/2.)
    p2 = Point(0,a/2.)
    p4 = Point(b,-a/2.)
    p3 = Point(b,a/2.)
    geom1 = Polygon([p4,p3,p2,p1])
    geom2 = Circle(Point(0.,0.),copening)
    geom3 = Circle(Point(b,0.),copening)
    mesh = generate_mesh(geom1-geom2-geom3,n_element)
    class Up(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[1], a/2.)

    class Bottom(SubDomain):
        def inside(self, x, on_boundary):
            return on_boundary and near(x[1], -a/2.)

    up = Up()
    bottom = Bottom()
    boundary_indicator = MeshFunction('size_t', mesh, 1, 0)
    up.mark(boundary_indicator, 1)
    bottom.mark(boundary_indicator, 2)
    return (mesh, boundary_indicator)

if __name__ == '__main__':
    (mesh, boundary_indicator) = Traction2dDefectSym()
    plot(boundary_indicator, interactive = True)