# Copyright (C) 2017 Corrado Maurini
#
# This file is part of sgdg-gradientdamage.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this repository. If not, see <http://www.gnu.org/licenses/>.

from dolfin import *
from mshr import *
import numpy as np
import math, sympy
import matplotlib.pyplot as plt
import os, shutil, uuid
from meshes import *
#parameters.plotting_backend='matplotlib'

#----------------------------------------------------------------------------
# Parameters
#----------------------------------------------------------------------------
user_par = Parameters("user", model = ("NS", ["LS", "NS"]), k = 2., eta=0.02, kres = 1.e-7, n_element = 100,  sym=1., suffix= "",root_dir="traction-2D")
user_par.add("tmin", 0.)
user_par.add("tmax", 1.)
user_par.add("nsteps", 50.)
user_par.add("u_scalar", 0.)
set_log_level(ERROR)
try:
    parameters.add(user_par)
except:
    print("user parameters already defined")
parameters.parse()
user_par = parameters.user
# Parameters of the nonlinear solver used for the alpha-problem
snes_solver_parameters_bounds = {"nonlinear_solver": "snes",
                          "symmetric": True,
                          "snes_solver": {"maximum_iterations": 100,
                                          "report": False,
                                          "line_search": "basic",
                                          "linear_solver": "umfpack",
                                          "method":"vinewtonrsls",
                                          "absolute_tolerance": 1e-9,
                                          "relative_tolerance": 1e-9,
                                          "error_on_nonconvergence": True}}
# Loading
u0 = Constant(0.0)
uL1 = Expression("t*a", t = 0., a=user_par.sym, degree=0)
uL2 = Expression("t/a", t = 0., a=user_par.sym, degree=0)
b = Constant(0.) # bulk load
# Parameters of the damage model
w_1, E_0, = 1., 1.
eta = user_par.eta # internal length
k = user_par.k
model = user_par.model
kres  = user_par.kres
# Constitutive functions of the damage model
if model == "LS":
    w = lambda z: 1- (1-z)**2
    E = lambda z: (1-w(z))/(1+(k-1)*w(z)) + Constant(kres)
elif model == "NS":
    w = lambda z: z
    E = lambda z: (1-z)**2 + Constant(kres)
n_el = user_par.n_element
if eta == 0.:
    prefix = "Local-%s-%s-%.3f"%(model,n_el,eta)
else:
    prefix = "DG-%s-%s-%.3f"%(model,n_el,eta)
#----------------------------------------------------------------------------
# Create the output files
#----------------------------------------------------------------------------
savedir = "%s/%s-%s"%(user_par.root_dir,prefix,user_par.suffix)
#savedir = "traction-2D/%s-%s-%s"%(user_par.sym,prefix,user_par.suffix)
if os.path.isdir(savedir):
    shutil.rmtree(savedir)
File(savedir+'/parameters.xml') << parameters
output_file_h5  = HDF5File(mpi_comm_world(), savedir+"/output.h5", "w")
output_file_xmdf = File(savedir+"/output.pvd")
time_data_file = open(savedir+"/data.txt", "w+")
time_data_file.write("%s %s %s %s %s %s %s \n" % ("t", "stress", "Length Damaged Zone", "Elastic Energy Local", "Elastic Energy Nonlocal", "Dissipated Energy", "Total Energy"))
damage_txt = open(savedir+"/data.txt", "w+")
strain_txt = open(savedir+"/data.txt", "w+")
#----------------------------------------------------------------------------
# Create mesh and boundary indicators
#----------------------------------------------------------------------------
(mesh, boundary_indicator) = CTmesh( a = .5, b = .5, c = .5, copening = 0.001, clength = .1, n_element=user_par.n_element)
#(mesh, boundary_indicator) = Traction2dDefect(a = .5, b = .7, copening = 0.1, n_element=user_par.n_element)
output_file_h5.write(mesh, "mesh")
#----------------------------------------------------------------------------
# Function space
#----------------------------------------------------------------------------
if user_par.u_scalar:
    V_u = FunctionSpace(mesh, "CG", 1)
else:
    V_u = VectorFunctionSpace(mesh, "CG", 1)
if eta == 0.:
    V_alpha = FunctionSpace(mesh, "DG", 0)
else:
    V_alpha = FunctionSpace(mesh, "CG", 1)
# Elasticity
u = Function(V_u)
u_test = TestFunction(V_u)
du = TrialFunction(V_u)
# Damage
alpha =  Function(V_alpha)
dalpha = TrialFunction(V_alpha)
alpha_test = TestFunction(V_alpha)
#----------------------------------------------------------------------------
# Define boundary conditions
#----------------------------------------------------------------------------
if user_par.u_scalar:
    V_u_bc = V_u
else:
    V_u_bc = V_u.sub(1)
bcu_0 = DirichletBC(V_u_bc, uL1, boundary_indicator, 1)
bcu_1 = DirichletBC(V_u_bc, uL2, boundary_indicator, 2)
#bcu_2 = DirichletBC(V_u, Constant(0.), boundary_indicator, 3)
bc_elasticity = [bcu_0,bcu_1]
bc_alpha_0 = DirichletBC(V_alpha, 0., boundary_indicator, 1)
bc_alpha_1 = DirichletBC(V_alpha, 0., boundary_indicator, 2)
bc_damage  = [bc_alpha_0,bc_alpha_1]
#----------------------------------------------------------------------------
# Initial value, lower and upper bounds for damage
#----------------------------------------------------------------------------
lb = interpolate(Expression("0.0", degree=0), V_alpha)
ub = interpolate(Constant(1.), V_alpha)
# Energy density
def eps(v):
    if user_par.u_scalar:
        eps = grad(v)
    else:
        eps = sym(grad(v))
    return eps
def sigma_0(v):
        if user_par.u_scalar:
            sigma = eps(v)
        else:
            nu = 0.3
            mu = 1./(2.0*(1.0 + nu))
            lmbda = 1.*nu/(1.0 - nu)**2
            sigma = 2.0*mu*(eps(v)) + lmbda*tr(eps(v))*Identity(2)
        return sigma
elastic_energy_local = 0.5*E(alpha)*inner(sigma_0(u),eps(u))
elastic_energy = elastic_energy_local
dissipated_energy = Constant(w_1) * (w(alpha) + (Constant(eta)**2)/2.*inner(grad(alpha),grad(alpha)))
strain_energy = elastic_energy + dissipated_energy
# Functional for the variational formulation
En = strain_energy*dx
#  Weak form of elasticity problem
En_u = derivative(En, u, u_test)
J_u = derivative(En_u, u, du)
weak_form_elasticity = replace(En_u, {u: du})
# Weak form of damage problem
En_alpha = derivative(En, alpha, alpha_test)
J_alpha = derivative(En_alpha, alpha, dalpha)
#----------------------------------------------------------------------------
# Set up the variational problems and solvers
#----------------------------------------------------------------------------
# Elasticity
problem_elasticity = LinearVariationalProblem(lhs(weak_form_elasticity), rhs(weak_form_elasticity), u, bc_elasticity)
solver_elasticity = LinearVariationalSolver(problem_elasticity)
solver_elasticity.parameters.linear_solver = "umfpack"
# Damage
problem_damage = NonlinearVariationalProblem(En_alpha, alpha, bc_damage, J_alpha)
solver_damage = NonlinearVariationalSolver(problem_damage)
solver_damage.parameters.update(snes_solver_parameters_bounds)
#----------------------------------------------------------------------------
# Solving
#----------------------------------------------------------------------------
alpha_error = Function(V_alpha)
maxiter = 1000
toll = 1.e-5
t = .0
loads = np.linspace(user_par.tmin, user_par.tmax, user_par.nsteps)
elastic_energy_out = 0*loads
dissipated_energy_out = 0*loads
stress_out = 0*loads
damaged_zone_length_out = 0*loads
output_file_h5.write(loads, "loads")
alpha_0 = Function(V_alpha)
errors = np.zeros((loads.size,maxiter))
# Time stepping
for (i, t) in enumerate(loads):
    #-----------------------------------
    # Step solver
    #-----------------------------------
    # parameters of the alternate minimization
    iter = 1; err_alpha = 1
    lap = 1; L_total = 0.0
    uL1.t = t
    uL2.t = -t
    print("-----------------------")
    print("Step t = %s")%t
    print("-----------------------")
    # Solving at each timestep with alternate minimization
    while err_alpha > toll and iter < maxiter:
        #plot(alpha,interactive=True)
        solver_elasticity.solve() # solve elastic problem
        problem_damage.set_bounds(lb.vector(),ub.vector())
        solver_damage.solve() # solve damage problem
        alpha_error.vector()[:] = alpha.vector() - alpha_0.vector()
        err_alpha = np.linalg.norm(alpha_error.vector().array(), ord = np.Inf) # test error
        alpha_0 = alpha.copy(deepcopy = True) # update
        print("Step %.3d/%.3d, t = %2.4e, iter: %s, error:%2.4e")%(i,loads.size,t,iter,err_alpha)
        errors[i,iter-1]=err_alpha
        iter=iter+1
    #plot(alpha, key = "alpha", title = "Damage at loading %.4f"%(t) ,range_min=0.,range_max=1., interactive = True )
    #updating the lower bound to account for the irreversibility
    np.savetxt(savedir+"/errors.txt",errors)
    lb.vector()[:] = alpha.vector()
    alpha.rename('alpha','damage')
    u.rename('u','displacement')
    damaged_zone_length = assemble(conditional(alpha > 1e-8, 1., 0.)*dx)
    # Save in hdmf5 format.
    output_file_h5.write(alpha, "alpha", t)
    output_file_h5.write(u, "u", t)
    output_file_xmdf << (u, t)
    output_file_xmdf << (alpha, t)
    #----------------------------------------------------------------------------
    # Some post-processing
    #----------------------------------------------------------------------------
    # Calculate the energies
    dissipated_energy_out[i] = assemble(dissipated_energy*dx) # dissipated energy
    elastic_energy_out[i] = assemble(elastic_energy_local*dx) #elastic energy
    ener_total = assemble(En) # total energy
    stress_out[i] = 0.
    damaged_zone_length_out[i] = damaged_zone_length
    time_data_file.write("%s %s %s %s %s %s \n" %(t, 0., damaged_zone_length, elastic_energy_out[i], dissipated_energy_out[i], ener_total))
output_file_h5.write(elastic_energy_out, "elastic_energy")
output_file_h5.write(dissipated_energy_out, "dissipated_energy")
output_file_h5.write(stress_out, "stress")
output_file_h5.write(damaged_zone_length_out, "damaged_zone")
time_data_file.close()
del output_file_h5, output_file_xmdf


