from fenics import *
parameters.plotting_backend = "matplotlib"
#parameters.form_compiler.log_level = 0
import matplotlib.pyplot as plt
import h5py
import os, sys
import numpy as np
sys.path.append("src/")
from postprocessing_local import *
import matplotlib
font = {'size'   : 14}
matplotlib.rc('font', **font)
plt.rc('text', usetex=False)

steps = [11,17,80]
for name in ["SG-NS-G1-0300-eta2.00e-01", "SG-LS-G1-0300-eta2.00e-01"]:
             #"SG-LS-GE-5.00e-01-5.00e-01-0300-eta5.00e-02"]:
    filename='results4paper/%s/output.h5'%name
    myHDF5File = HDF5File(mpi_comm_world(),filename, "r")
    fileh5py = h5py.File(filename,'r')
    mesh=Mesh()
    myHDF5File.read(mesh,"mesh",False)
    u = initialize_cg1(myHDF5File)
    eps = initialize_cg1(myHDF5File)
    tau = initialize_dg0(myHDF5File)
    alpha = initialize_dg0(myHDF5File)
    loads = fileh5py.get('loads').value
    fig = plt.figure(4)
    fig,ax = plt.subplots(len(steps),1,sharex=True)
    ax_glob = fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    #plt.xlabel("x")
    ax_globt=ax_glob.twinx()
    ax_glob.set_ylabel("Damage")
    ax_globt.set_ylabel("Strain")
    ax_glob.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_globt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_glob.grid(False)
    ax_globt.grid(False)
    #plt.rc('font', family='serif')
    for (i,step) in enumerate(steps):
        myHDF5File.read(alpha, "alpha/vector_%d"%step)
        myHDF5File.read(eps, "eps/vector_%d"%step)
        strain = ax[i].plot(mesh.coordinates(),alpha.compute_vertex_values(),color='black',ls="solid",marker ="", linewidth=3.0, label = "Damage")
        ax0 = ax[i]
        ax0.set_title(r't = %.2f'%loads[step],y=0.08,fontsize="large")
        ax0.set_ylim(ymin=0,ymax=1.1)
        ax0.locator_params(axis='y',nbins=3)
        ax0t = ax[i].twinx()
        damage = ax0t.plot(mesh.coordinates(),eps.compute_vertex_values(),color='gray',ls="dashed",marker ="",linewidth=3.0, label = "Strain")
        ax0t.yaxis.grid(False)
        ax0t.set_ylim(ymin=0)
        ax0t.locator_params(axis='y',nbins=3)
        lns = strain+damage
        labs = [l.get_label() for l in lns]
    plt.legend(lns, labs, loc ='upper center',bbox_to_anchor = (0,0,1,1),ncol=2,
               bbox_transform = plt.gcf().transFigure)
     #fig.tight_layout()
    plt.savefig("figures/fields-%s.pdf"%name)
    #plt.show()
    del fileh5py, myHDF5File

steps = [10]
for name in ["SG-NS-GE-0050-eta2.00e-01"]:
             #"SG-LS-GE-5.00e-01-5.00e-01-0300-eta5.00e-02"]:
    filename='results4paper/%s/output.h5'%name
    myHDF5File = HDF5File(mpi_comm_world(),filename, "r")
    fileh5py = h5py.File(filename,'r')
    mesh=Mesh()
    myHDF5File.read(mesh,"mesh",False)
    u = initialize_cg1(myHDF5File)
    eps = initialize_cg1(myHDF5File)
    tau = initialize_dg0(myHDF5File)
    alpha = initialize_dg0(myHDF5File)
    loads = fileh5py.get('loads').value
    fig,ax = plt.subplots(1,1,sharex=True,figsize=(6,3))
    ax_glob = fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    #plt.xlabel("x")
    ax_globt = ax_glob.twinx()
    ax_glob.set_ylabel("Damage")
    ax_globt.set_ylabel("Strain")
    ax_glob.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_globt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_glob.grid(False)
    ax_globt.grid(False)
    for (i,step) in enumerate(steps):
        myHDF5File.read(alpha, "alpha/vector_%d"%step)
        myHDF5File.read(eps, "eps/vector_%d"%step)
        strain = ax.plot(mesh.coordinates(),alpha.compute_vertex_values(),color='black',ls="solid",marker ="", linewidth=3.0, label = "Damage")
        ax0=ax
        ax0.set_title(r'NS-G0, n=50'%loads[step],y=0.08,fontsize="large")
        ax0.set_ylim(ymin=0,ymax=1.1)
        ax0.locator_params(axis='y',nbins=3)
        ax0t = ax.twinx()
        damage = ax0t.plot(mesh.coordinates(),eps.compute_vertex_values(),color='gray',ls="dashed",marker ="",linewidth=3.0, label = "Strain")
        ax0t.yaxis.grid(False)
        ax0t.set_ylim(ymin=0)
        ax0t.locator_params(axis='y',nbins=3)
        lns = strain+damage
        labs = [l.get_label() for l in lns]
    plt.legend(lns, labs, loc ='center',bbox_to_anchor = (0,0,1,1),ncol=2,
               bbox_transform = plt.gcf().transFigure)
    #fig.tight_layout()
    ax.set_aspect("auto")
    plt.savefig("figures/fields-%s.pdf"%name)
    #plt.show()
    del fileh5py, myHDF5File

steps = [10]
for name in ["SG-LS-GE-0050-eta2.00e-01"]:
             #"SG-LS-GE-5.00e-01-5.00e-01-0300-eta5.00e-02"]:
    filename='results4paper/%s/output.h5'%name
    myHDF5File = HDF5File(mpi_comm_world(),filename, "r")
    fileh5py = h5py.File(filename,'r')
    mesh=Mesh()
    myHDF5File.read(mesh,"mesh",False)
    u = initialize_cg1(myHDF5File)
    eps = initialize_cg1(myHDF5File)
    tau = initialize_dg0(myHDF5File)
    alpha = initialize_dg0(myHDF5File)
    loads = fileh5py.get('loads').value
    fig,ax = plt.subplots(1,1,sharex=True,figsize=(6,3))
    ax_glob = fig.add_subplot(111, frameon=False)
    # hide tick and tick label of the big axes
    #plt.xlabel("x")
    ax_globt = ax_glob.twinx()
    ax_glob.set_ylabel("Damage")
    ax_globt.set_ylabel("Strain")
    ax_glob.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_globt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
    ax_glob.grid(False)
    ax_globt.grid(False)
    #plt.rc('font', family='serif')
    for (i,step) in enumerate(steps):
        myHDF5File.read(alpha, "alpha/vector_%d"%step)
        myHDF5File.read(eps, "eps/vector_%d"%step)
        strain = ax.plot(mesh.coordinates(),alpha.compute_vertex_values(),color='black',ls="solid",marker ="", linewidth=3.0, label = "Damage")
        ax0=ax
        ax0.set_title(r'LS-G0, n = 50', y=0.08,fontsize="large")
        ax0.set_ylim(ymin=0,ymax=1.1)
        ax0.locator_params(axis='y',nbins=3)
        ax0t = ax.twinx()
        damage = ax0t.plot(mesh.coordinates(),eps.compute_vertex_values(),color='gray',ls="dashed",marker ="",linewidth=3.0, label = "Strain")
        ax0t.yaxis.grid(False)
        ax0t.set_ylim(ymin=0)
        ax0t.locator_params(axis='y',nbins=3)
        lns = strain+damage
        labs = [l.get_label() for l in lns]
    plt.legend(lns, labs, loc ='center',bbox_to_anchor = (0,0,1,1),ncol=2,
               bbox_transform = plt.gcf().transFigure)
    #fig.tight_layout()
    ax.set_aspect("auto")
    plt.savefig("figures/fields-%s.pdf"%name)
    #plt.show()
    del fileh5py, myHDF5File