# python sgdam2D.py --user.nsteps 100 --user.n_element 50 --user.model LS --user.G 1
# python sgdam2D.py --user.nsteps 100 --user.n_element 150 --user.model LS
#python src/sgdam.py \
#        --user.model NS \
#        --user.kresE 1.e-8 --user.kresG 0.\
#        --user.imp_pos 0.2  \
#        --user.k 2. --user.root='results/test3' \
#        --user.imp_d 0.1 --user.imp_ampl 0.1  \
#        --user.n_element 150 --user.suffix test --user.nsteps 50 \
#        --user.tmax 5 --user.G 0 --user.eta 1.0e-01
#
import subprocess
import os, sys
import numpy as np
from fenics import *
sys.path.append("src")
from sgdam import *

user_par = Parameters("user", model=("NS",["LS","NS"]),
                          G=1, n_element=20, eta=0.2, k=2.,
                          kresE=1.e-5, kresG=1.e-8,
                          imp_d=0.0, imp_ampl=0.0, imp_pos=0.4,
                          suffix="", root_dir="results4paper",
                          tmin = 0., tmax = 8., nsteps = 81)

nel_values = np.array([30,40,50,60,70,80,90,100,125,150,175,200,250,300])# 70, 300, 400, 500])#[ 20, 30, 50, 60, 80, 100, 200])
#basic_options = "--user.imp_pos 0.1  --user.root_dir='results4paper' "
for model in ["LS", "NS"]:
    for nel in nel_values:
        user_par.model = model
        print(model, " - ", nel)
        user_par.n_element = int(nel)
        #user_par.suffix =  """%04.02e-%04.02e-%.4d-eta%04.02e"%(user_par.imp_d,user_par.imp_ampl,user_par.n_element,user_par.eta)
        user_par.suffix =  "%.4d-eta%04.02e"%(user_par.n_element,user_par.eta)
        sg_solver(user_par)


